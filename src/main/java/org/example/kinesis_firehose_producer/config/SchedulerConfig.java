package org.example.kinesis_firehose_producer.config;

import lombok.RequiredArgsConstructor;
import org.example.kinesis_firehose_producer.task.GeneratorTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@RequiredArgsConstructor
public class SchedulerConfig {

    private final DemoProperties demoProperties;
    private final GeneratorTask generatorTask;

    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.initialize();
        taskScheduler.scheduleAtFixedRate(generatorTask, demoProperties.getScheduleRateMilliseconds());
        return taskScheduler;
    }

}
