package org.example.kinesis_firehose_producer.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("demo")
public class DemoProperties {

    private long scheduleRateMilliseconds;
    private String deliveryStreamName;
    private String awsRegion;

}
