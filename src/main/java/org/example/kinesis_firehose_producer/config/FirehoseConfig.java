package org.example.kinesis_firehose_producer.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.firehose.FirehoseClient;

@Configuration
@RequiredArgsConstructor
public class FirehoseConfig {

    private final DemoProperties demoProperties;

    @Bean
    public FirehoseClient firehoseClient() {
        return FirehoseClient.builder()
                .region(Region.of(demoProperties.getAwsRegion()))
                .build();
    }
}
