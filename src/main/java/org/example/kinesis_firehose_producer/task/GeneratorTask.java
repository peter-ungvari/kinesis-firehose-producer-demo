package org.example.kinesis_firehose_producer.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.example.kinesis_firehose_producer.config.DemoProperties;
import org.example.kinesis_firehose_producer.model.LoginEvent;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.services.firehose.FirehoseClient;
import software.amazon.awssdk.services.firehose.model.PutRecordRequest;
import software.amazon.awssdk.services.firehose.model.Record;

@Component
@RequiredArgsConstructor
@Slf4j
public class GeneratorTask implements Runnable {

    private final ObjectMapper objectMapper;
    private final FirehoseClient firehoseClient;
    private final LoginEventGenerator loginEventGenerator;
    private final DemoProperties demoProperties;

    @SneakyThrows
    @Override
    public void run() {
        LoginEvent loginEvent = loginEventGenerator.generate();
        String data = objectMapper.writeValueAsString(loginEvent);
        PutRecordRequest putRecordRequest = PutRecordRequest.builder()
                .deliveryStreamName(demoProperties.getDeliveryStreamName())
                .record(Record.builder().data(SdkBytes.fromUtf8String(data)).build())
                .build();
        firehoseClient.putRecord(putRecordRequest);
        log.info("Record put to delivery stream: {}", data);
    }
}
