package org.example.kinesis_firehose_producer.task;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.example.kinesis_firehose_producer.model.LoginEvent;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Random;

@Component
@Slf4j
public class LoginEventGenerator {

    private final Random random = new Random();
    private static final String[] DEVICES = {"BIGSCREEN", "DESKTOP", "IOS", "ANDROID"};

    public LoginEvent generate() {
        Faker faker = new Faker();
        LoginEvent loginEvent = new LoginEvent(
                faker.internet().emailAddress(),
                Instant.now(),
                faker.country().countryCode2(),
                DEVICES[random.nextInt(4) % 4],
                faker.internet().ipV4Address());
        log.info("LoginEvent generated: {}", loginEvent);
        return loginEvent;
    }

}
