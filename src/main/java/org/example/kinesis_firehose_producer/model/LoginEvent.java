package org.example.kinesis_firehose_producer.model;

import lombok.Value;

import java.time.Instant;

@Value
public class LoginEvent {

    String email;
    Instant loginTimestamp;
    String territory;
    String deviceType;
    String deviceIp;

}
